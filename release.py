import json
import subprocess
import time
import sys, getopt
import argparse
import urllib
from urllib2 import Request, urlopen

PROJECT_ID = "10138587"
has_demo = False
has_latest = False

# bump version
def bump_version(type):
    with open('package.json') as data_file:
        data = json.load(data_file)
    version = data['version']
    version_arr = version.split('.')
    if type == 'minor':
        new_version = version_arr[0] +"."+ str(int(version_arr[1]) +1) +"."+ version_arr[2]
    else:
        new_version = str(int(version_arr[0])+1) + ".0.0"

    # Safely read the input filename using 'with'
    filename = 'package.json'
    with open(filename) as f:
        s = f.read()
        if version not in s:
            print '"{version}" not found in {filename}.'.format(**locals())
            return
        else:
            print '"{version}" found in {filename}.'.format(**locals())
            # Safely write the changed content, if found in the file
            with open(filename, 'w') as f:
                print 'Bumping version "{version}" to "{new_version}" in {filename}'.format(**locals())
                s = s.replace(version, new_version)
                f.write(s)
        
        time.sleep(1)
        tag_new_version(new_version)

def tag_new_version(new_version):
    subprocess.call(["git", "add", "."])
    subprocess.call(["git", "commit", "-m", "#bump version"])
    subprocess.call(["git", "tag", "v"+str(new_version)])

def parse_args():
   releasetype = 'minor'
   parser = argparse.ArgumentParser()
   parser.add_argument('-major', '--major', dest='major', action='store_true')
   parser.add_argument('-minor', '--minor', dest='minor', action='store_true')
   args = parser.parse_args()

   if args.major:
       releasetype = 'major'
   elif args.minor:
       releasetype = 'minor'

   print "releasing in "+releasetype+" release..."
   bump_version(releasetype)

def console(cmd):
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    out, err = p.communicate()
    return (p.returncode, out, err)

# gitlab api   
def list_tags():
    req = Request('https://gitlab.com/api/v4/projects/'+PROJECT_ID+'/repository/tags')
    req.add_header('PRIVATE-TOKEN', 'Ywxwr5AcFCMBsWAxkUHx')
    resp = urlopen(req)
    content = resp.read()
    data = json.loads(content)
    print data[0]['name']

def rollback_demo():
    find_rollback_demo()
    if has_demo:
        print('Deleting demo tags....')
        req = Request('https://gitlab.com/api/v4/projects/'+PROJECT_ID+'/repository/tags/demo')
        req.add_header('PRIVATE-TOKEN', 'Ywxwr5AcFCMBsWAxkUHx')
        req.get_method = lambda: 'DELETE'
        resp = urlopen(req)
        content = resp.read()
        subprocess.call(["git", "tag", "--delete", "demo"])
    
    print('Creating demo_rollback tag....')
    refs = console("git rev-parse HEAD")[1]
    refs = refs[:-1]
    print(refs)
    subprocess.call(["git", "tag", "demo_rollback"])
    req = Request('https://gitlab.com/api/v4/projects/'+PROJECT_ID+'/repository/tags')
    values = {'ref' : refs,'tag_name' : 'demo_rollback'}
    data = urllib.urlencode(values)
    req.add_header('PRIVATE-TOKEN', 'Ywxwr5AcFCMBsWAxkUHx')
    req.get_method = lambda: 'POST'
    resp = urlopen(req, data)
    content = resp.read()
    print(data)

def rollback_latest():
    find_rollback_latest()
    if has_latest:
        print('Deleting latest tags....')
        req = Request('https://gitlab.com/api/v4/projects/'+PROJECT_ID+'/repository/tags/latest')
        req.add_header('PRIVATE-TOKEN', 'Ywxwr5AcFCMBsWAxkUHx')
        req.get_method = lambda: 'DELETE'
        resp = urlopen(req)
        content = resp.read()
        subprocess.call(["git", "tag", "--delete", "latest"])
    
    print('Creating latest_rollback tag....')
    refs = console("git rev-parse HEAD")[1]
    refs = refs[:-1]
    print(refs)
    subprocess.call(["git", "tag", "latest_rollback"])
    req = Request('https://gitlab.com/api/v4/projects/'+PROJECT_ID+'/repository/tags')
    values = {'ref' : refs,'tag_name' : 'latest_rollback'}
    data = urllib.urlencode(values)
    req.add_header('PRIVATE-TOKEN', 'Ywxwr5AcFCMBsWAxkUHx')
    req.get_method = lambda: 'POST'
    resp = urlopen(req, data)
    content = resp.read()
    print(data)

def find_rollback_demo():
    req = Request('https://gitlab.com/api/v4/projects/'+PROJECT_ID+'/repository/tags')
    req.add_header('PRIVATE-TOKEN', 'Ywxwr5AcFCMBsWAxkUHx')
    resp = urlopen(req)
    content = resp.read()
    data = json.loads(content)

    for i in data:
        if i['name'] == 'demo_rollback':
            print('Deleting demo_rollback tags....')
            req = Request('https://gitlab.com/api/v4/projects/'+PROJECT_ID+'/repository/tags/demo_rollback')
            req.add_header('PRIVATE-TOKEN', 'Ywxwr5AcFCMBsWAxkUHx')
            req.get_method = lambda: 'DELETE'
            resp = urlopen(req)
            content = resp.read()
            subprocess.call(["git", "tag", "--delete", "demo_rollback"])

def find_rollback_latest():
    req = Request('https://gitlab.com/api/v4/projects/'+PROJECT_ID+'/repository/tags')
    req.add_header('PRIVATE-TOKEN', 'Ywxwr5AcFCMBsWAxkUHx')
    resp = urlopen(req)
    content = resp.read()
    data = json.loads(content)

    for i in data:
        if i['name'] == 'latest_rollback':
            print('Deleting latest_rollback tags....')
            req = Request('https://gitlab.com/api/v4/projects/'+PROJECT_ID+'/repository/tags/latest_rollback')
            req.add_header('PRIVATE-TOKEN', 'Ywxwr5AcFCMBsWAxkUHx')
            req.get_method = lambda: 'DELETE'
            resp = urlopen(req)
            content = resp.read()
            subprocess.call(["git", "tag", "--delete", "latest_rollback"])                

def check_demo_latest():
    req = Request('https://gitlab.com/api/v4/projects/'+PROJECT_ID+'/repository/tags')
    req.add_header('PRIVATE-TOKEN', 'Ywxwr5AcFCMBsWAxkUHx')
    resp = urlopen(req)
    content = resp.read()
    data = json.loads(content)

    for i in data:
        if i['name'] == 'demo':
            has_demo = True
        if i['name'] == 'latest':
            has_latest = True    
    

# main
if __name__ == "__main__":
    check_demo_latest()
    rollback_demo()
    rollback_latest()
    parse_args()